import os

debug = os.getenv("debug", default=False)

JSONPH_ENV = {"domain": "https://jsonplaceholder.typicode.com"}

MOCKI_ENV = {"domain": "https://api.mocki.io/v1"}
