import logging
from typing import Union

import requests
from jsonschema import validate


def validate_response_code(
    response: requests.Response,
    expected_code: Union[int, list],
    status_range=False,
    message=None,
) -> None:
    """
    Asserts the response status code is equal to one of the expected codes supplied
    """
    # If no message is provided, make one up
    if not message:
        message = (
            f"Expected status code was {expected_code}, found {response.status_code}"
        )

    # If validating for an exact code, return error if they don't match
    if type(expected_code) == int:
        if status_range:
            assert expected_code <= response.status_code < expected_code + 99, message
        else:
            assert response.status_code == expected_code, message

    # If looking for any number of codes, return error if they don't match at least one of them
    elif type(expected_code) == list:
        result = False
        for code in expected_code:
            if status_range:
                if code <= response.status_code < code + 99:
                    result = True
                    break
            else:
                result = response.status_code == code

        assert result, message


def validate_response_headers(response: requests.Response, content_type: str = "json"):
    """
    Asserts the response headers match the expected content_type
    """
    # If a response or expected headers are not present, return error
    if not response.headers:
        raise ValueError("Response required")

    expected_headers = {}

    match content_type.lower():
        case "json":
            expected_headers["Content-Type"] = "application/json; charset=utf-8"
        case "plain":
            expected_headers["Content-Type"] = "text/plain; charset=utf-8"
        case "html":
            expected_headers["Content-Type"] = "text/html; charset=utf-8"
        case "xml":
            expected_headers["Content-Type"] = "application/xml; charset=utf-8"
        case "octet-stream":
            expected_headers["Content-Type"] = "application/octet-stream"
        case _:
            logging.getLogger().error(
                f"Unknown content-type '{content_type}' provided."
            )
            raise ValueError(f"Unknown content-type '{content_type}' provided.")

    if type(expected_headers) is dict:
        for key, value in expected_headers.items():
            assert (
                key in response.headers
            ), f"{key} not found in {response.headers.values()}"
            assert (
                value in response.headers.values()
            ), f"{value} not found in {response.headers.values()}"
    elif type(expected_headers) is list:
        for header in expected_headers:
            assert (
                header in response.headers
            ), f"{header} not found in {response.headers}"
    elif type(expected_headers) is str:
        assert (
            expected_headers in response.headers
        ), f"{expected_headers} not found in {response.headers}"


def validate_response_time(response: requests.Response, expected_time: int = 1000):
    """
    Asserts if the response time is lower than the expected_time value.
    """
    elapsed_time = int(
        round(response.elapsed.microseconds) / 1000
    )  # Convert elapsed time to ms
    assert (
        elapsed_time <= expected_time
    ), f"Request took {elapsed_time}ms, expected {expected_time}ms"


def validate_response_schema(response: requests.Response = None, schema: dict = None):
    """
    Uses JSONSchema.validate to check if two schemas match.  Asserts if they do not.
    """
    if not response:
        raise AttributeError("Unable to validate empty response")

    validate(instance=response.json(), schema=schema)
