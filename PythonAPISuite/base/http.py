import requests
from requests.utils import requote_uri

from .util import Settings

current_session = requests.Session()


def prepare_request(
    method,
    uri,
    headers=None,
    headers_override=True,
    params=None,
    session=current_session,
    **kwargs,
) -> requests:
    """
    Build and execute HTTP requests
    """
    if not uri.startswith("http"):
        url = f"{Settings.get_environment_value('domain')}{requote_uri(uri)}"
    else:
        url = uri

    if not headers and headers_override:
        headers = common_headers()

    requests.Request(method, url=url, headers=headers, params=params, **kwargs)

    response = session.request(
        method, url=url, headers=headers, params=params, **kwargs
    )

    return response


def common_headers(headers=None, default=True) -> dict:
    """
    Defaults to application/json unless default option set to False.
    """
    all_headers = {}

    if default:
        all_headers["Content-Type"] = "application/json"

    if headers:
        all_headers.update(headers)

    return all_headers


def get(uri, headers=None, params=None, session=current_session, **kwargs) -> requests:
    return prepare_request(
        "get", uri, headers=headers, params=params, session=session, **kwargs
    )


def delete(
    uri, headers=None, params=None, session=current_session, **kwargs
) -> requests:
    return prepare_request(
        "delete", uri, headers=headers, params=params, session=session, **kwargs
    )


def post(uri, headers=None, params=None, session=current_session, **kwargs) -> requests:
    return prepare_request(
        "post", uri, headers=headers, params=params, session=session, **kwargs
    )


def put(uri, headers=None, params=None, session=current_session, **kwargs) -> requests:
    return prepare_request(
        "put", uri, headers=headers, params=params, session=session, **kwargs
    )


def patch(
    uri, headers=None, params=None, session=current_session, **kwargs
) -> requests:
    return prepare_request(
        "patch", uri, headers=headers, params=params, session=session, **kwargs
    )
