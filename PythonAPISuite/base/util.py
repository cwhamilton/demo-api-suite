import json
import logging
import ntpath
import os
import random
import shutil
import string
import time
from calendar import calendar
from datetime import datetime

import requests
from dateutil.parser import parse as date_parse


class Util:
    @classmethod
    def __new__(cls, *args, **kwargs):
        raise TypeError("Util should never get instantiated")

    @staticmethod
    def get_alpha_num(length: int = 10, alpha_num_type: str = "mix") -> str:
        """
        Returns a random string based on length and type.
        """
        alpha_num = ""

        if alpha_num_type == "lower":
            case = string.ascii_lowercase
        elif alpha_num_type == "upper":
            case = string.ascii_uppercase
        elif alpha_num_type == "mix":
            case = string.ascii_letters + string.digits
        else:
            case = string.ascii_letters

        return alpha_num.join(random.choice(case) for _ in range(length))

    @staticmethod
    def get_unique_string(num: int = 1, char_count: int = 7) -> [list, str]:
        if num < 1:
            num = 1

        if num == 1:
            return Util.get_alpha_num(char_count, "lower")

        if num > 1:
            names = []
            for n in range(0, num):
                names.append(Util.get_alpha_num(char_count, "lower"))
            return names

    @staticmethod
    def get_epoch_time(
        milli: bool = False, sec: bool = False, date: [str, int, float] = None
    ) -> [int, dict]:
        if not date:
            return_time = time.time()  # Now
        else:
            date = date_parse(date)
            return_time = calendar.timegm(date.timetuple())

        if sec and milli:
            return [int(return_time), int(return_time) * 1000]
        elif sec:
            return int(time.time())
        elif milli:
            return int(time.time()) * 1000
        else:
            return 0

    @staticmethod
    def dict_compare(dict_1: dict, dict_2: dict):
        """
        Compares two dictionaries.

        dict_1 = new dictionary
        dict_2 = base dictionary

        Returns:
            Added:      Keys in dict_1 that are not in dict_2
            Removed:    Keys in dict_2 that are not in dict_1
            Same:       Keys in dict_2 that are the same value in dict_1
            Diff:       Keys in dict_2 that are not the same value in dict_1

        Example:
            dict_1 = {"a": 1, "b": 2, "c": 3}
            dict_2 = {"b": 2, "c": 4, "d": 5}

            added    = {'a'}
            removed  = {'d'}
            same     = {'b'}
            diff     = {'c'}
        """
        dict_1_keys = set(dict_1.keys())
        dict_2_keys = set(dict_2.keys())
        shared_keys = dict_1_keys.intersection(dict_2_keys)

        added = dict_1_keys - dict_2_keys
        removed = dict_2_keys - dict_1_keys
        same = set(o for o in shared_keys if dict_1[o] == dict_2[o])
        diff = set(o for o in shared_keys if dict_1[o] != dict_2[o])

        return locals()

    @staticmethod
    def get_request_method(response: requests.Response) -> str:
        """
        Returns the request method, GET/POST/PUT, etc
        """
        return str(response.request)[
            str(response.request).find("[") + 1 : str(response).find("]")
        ]

    @staticmethod
    def is_response_expected_status_code(
        response: requests.Response, expected: int
    ) -> None:
        """
        Checks if response is expected status code, if it is not, it throws an error.
        """
        if response.status_code != expected:
            logging.getLogger().error(
                f"{Util.get_request_method(response)} {response.url} returned "
                f"{response.status_code}, expected {expected}"
            )
            raise ValueError(
                f"{Util.get_request_method(response)} {response.url} returned "
                f"{response.status_code}, expected {expected}"
            )


class Settings:
    authenticated = False
    environment = None

    @staticmethod
    def get_environment_value(property_name=None, default_value=None):
        if not Settings.environment:
            raise AttributeError()
        else:
            return Settings.environment.get(property_name, default_value)


class FileHandle:
    def __init__(self, handle_name, file_name):
        self.handle = handle_name
        self.name = file_name

    def path_leaf(self):
        head, tail = ntpath.split(self.name)
        return tail or ntpath.basename(head)

    def file_info(self):
        return {self.handle: (self.path_leaf(), open(self.name, "rb"))}


class DataLoader:
    JSON = "json"
    TXT = "txt"
    DATA = "dat"
    ZIP = "zip"
    TMP_FOLDER = "tmp_data/"
    BASE_FOLDER = "data_files"

    supported_data_types = [JSON, TXT, DATA]

    def load_data(self, file=None, data_type=None):
        """
        Loads a data file.  Returns a file according to given type
        :param file: File Name (Relative path)
        :param data_type: Data type
            JSON -> Dict
            Text -> String
            Data -> Byte array
            Zip  -> Zip file
        :return:
        """
        if not file or not data_type:
            raise AttributeError()

        if data_type not in self.supported_data_types:
            raise AttributeError()

        data = None

        file = self.resolve_file_path(file)

        mode = "r"
        if data_type == self.DATA or data_type == self.ZIP:
            mode = "rb"

        tmp_file = self.copy_file_to_tmp_space(file)

        with open(tmp_file, mode) as data_file:
            if data_type == self.JSON:
                data = json.load(data_file)
            if data_type == self.TXT:
                data = data_file.readlines()
            if data_type == self.DATA or data_type == self.ZIP:
                data = data_file.read()

        return data

    def resolve_file_path(self, file) -> str:
        """
        Resolves file path, appends BASE_FOLDER if not provided
        """
        if not file.startswith(self.BASE_FOLDER):
            file = os.path.join(self.BASE_FOLDER, file)

        return file

    def get_file_handler(self, handle_name, file_name) -> FileHandle:
        file_name = self.resolve_file_path(file_name)
        source_file = self.copy_file_to_tmp_space(file_name)

        return FileHandle(handle_name, source_file)

    def copy_file_to_tmp_space(self, file_name: str) -> [str, None]:
        """
        Copies requested file to a temp folder, original file can't be changed this way.
        Returns new path
        """
        if not file_name:
            return None

        file_name = self.resolve_file_path(file_name)

        now = datetime.now()
        ts = now.strftime("%Y%m%d%H%M%S%f")

        target_file_name = os.path.join(
            os.getcwd(), self.TMP_FOLDER, ts, "_", os.path.basename(file_name)
        )
        shutil.copy(file_name, target_file_name)

        return target_file_name
