pytest~=6.2.4
requests==2.27.1
jsonschema==3.2.0
python-dateutil~=2.8.1
locust==2.6.0