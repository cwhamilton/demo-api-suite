import logging

from locust import HttpUser, task, tag, between, events, TaskSet
from locust.runners import MasterRunner

from PythonAPISuite.base import assertions


@events.quitting.add_listener
def _(environment):
    """
    This will automatically stop the test pass if failures exceed criteria listed below
    """
    if environment.stats.total.fail_ratio > 0.01:
        logging.error("Tests stopped due to failure rate > 1%")
        environment.process_exit_code = 1
    if environment.stats.total.avg_response_time > 1000:
        logging.error("Tests stopped due to average response time > 1000ms")
        environment.process_exit_code = 1
    if environment.stats.total.get_response_time_percentile(0.95) > 800:
        logging.error(
            "Tests stopped due to more than 5% tests response time over 800ms"
        )
        environment.process_exit_code = 1
    else:
        environment.process_exit_code = 0


@events.test_start.add_listener
def on_test_start(environment, **kwargs):
    """
    Runs once before each test pass
    """
    print("Test Starting")
    print(
        f"Current Average Response Time:   |  {round(environment.stats.total.avg_response_time, 2)}ms"
    )


@events.init.add_listener
def on_locust_init(environment, **kwargs):
    """
    Triggered at the start of each Locust process.  Useful in distributed mode where each worker process (not user)
        needs to initialize.
    """
    if isinstance(environment.runner, MasterRunner):
        print("I am on Master node")
    else:
        print("On a worker or standalone node")


@events.test_stop.add_listener
def on_test_stop(environment, **kwargs):
    """
    Runs once after each test pass
    """
    print("Test Complete")
    print(
        f"Min Response Time:       |  {round(environment.stats.total.min_response_time, 2)}ms"
    )
    print(
        f"Average Response Time:   |  {round(environment.stats.total.avg_response_time, 2)}ms"
    )
    print(
        f"Max Response Time:       |  {round(environment.stats.total.max_response_time, 2)}ms"
    )
    print(f"Number of Requests Made: |  {environment.stats.num_requests}")
    print(f"Total Errors:            |  {environment.stats.num_failures}")


@events.request.add_listener
def on_request(context, **kwargs):
    """
    Runs per request, can be useful for logging.
    Not much going on in this API though so leaving empty.
    """
    pass


class JSONPHLoadPhotosTests(TaskSet):
    wait_time = between(1, 2.5)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._album_id = None
        self._comment_id = None
        self._photo_id = None
        self._post_id = None
        self._todo_id = None
        self._user_id = None

    def on_start(self):
        """
        Setup.
        Runs once before load tests are executed
        """
        self._photo_id = self.client.get("/photos").json()[0].get("id")

    def on_stop(self):
        """
        Teardown.
        Runs once after load tests are completed.
        """
        pass

    @task
    @tag("photos")
    def get_photos(self):
        response = self.client.get("/photos")

        assertions.validate_response_code(response, 200)
        assertions.validate_response_time(response)

    @task
    @tag("photos")
    def get_photo_id(self):
        response = self.client.get(f"/photos/{self._photo_id}")

        assertions.validate_response_code(response, 200)
        assertions.validate_response_time(response)


class JSONPHLoadUsersTests(TaskSet):
    wait_time = between(1, 2.5)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._album_id = None
        self._user_id = None

    def on_start(self):
        """
        Setup.
        Runs once before load tests are executed
        """
        self._user_id = self.client.get("/users").json()[0].get("id")

    def on_stop(self):
        """
        Teardown.
        Runs once after load tests are completed.
        """
        pass

    @task
    @tag("users")
    def get_users(self):
        response = self.client.get("/users")

        assertions.validate_response_code(response, 200)
        assertions.validate_response_time(response)

    @task
    @tag("users")
    def get_user_id(self):
        response = self.client.get(f"/users/{self._user_id}")

        assertions.validate_response_code(response, 200)
        assertions.validate_response_time(response)


class UsersUser(HttpUser):
    tasks = [JSONPHLoadUsersTests]
    min_wait = 100
    max_wait = 5000


class PhotosUsers(HttpUser):
    tasks = [JSONPHLoadPhotosTests]
    min_wait = 100
    max_wait = 5000
