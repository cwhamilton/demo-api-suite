GET_POSTS = {
    "type": "array",
    "items": [
        {
            "properties": {
                "userId": {"type": "integer"},
                "id": {"type": "integer"},
                "title": {"type": "string"},
                "body": {"type": "string"},
            }
        }
    ],
}

GET_POST = {
    "type": "object",
    "required": ["userId", "id", "title", "body"],
    "properties": {
        "userId": {"type": "integer"},
        "id": {"type": "integer"},
        "title": {"type": "string"},
        "body": {"type": "string"},
    },
}

GET_POST_COMMENTS = {
    "type": "array",
    "items": [
        {
            "type": "object",
            "properties": {
                "postId": {"type": "integer"},
                "id": {"type": "integer"},
                "name": {"type": "string"},
                "email": {"type": "string"},
                "body": {"type": "string"},
            },
            "required": ["postId", "id", "name", "email", "body"],
        }
    ],
}

GET_COMMENTS = {
    "type": "array",
    "items": [
        {
            "type": "object",
            "properties": {
                "postId": {"type": "integer"},
                "id": {"type": "integer"},
                "name": {"type": "string"},
                "email": {"type": "string"},
                "body": {"type": "string"},
            },
            "required": ["postId", "id", "name", "email", "body"],
        }
    ],
}

PUT_POST = {
    "type": "object",
    "properties": {
        "title": {"type": "string"},
        "body": {"type": "string"},
        "userId": {"type": "integer"},
        "id": {"type": "integer"},
    },
    "required": ["title", "body", "userId", "id"],
}

GET_ALBUMS = {
    "type": "array",
    "items": [
        {
            "type": "object",
            "properties": {
                "userId": {"type": "integer"},
                "id": {"type": "integer"},
                "title": {"type": "string"},
            },
        }
    ],
}

GET_ALBUM = {
    "type": "object",
    "properties": {
        "userId": {"type": "integer"},
        "id": {"type": "integer"},
        "title": {"type": "string"},
    },
    "required": ["userId", "id", "title"],
}

GET_PHOTOS = {
    "type": "array",
    "items": [
        {
            "type": "object",
            "properties": {
                "albumId": {"type": "integer"},
                "id": {"type": "integer"},
                "title": {"type": "string"},
                "url": {"type": "string"},
                "thumbnailUrl": {"type": "string"},
            },
            "required": ["albumId", "id", "title", "url", "thumbnailUrl"],
        }
    ],
}

GET_PHOTO = {
    "type": "object",
    "properties": {
        "albumId": {"type": "integer"},
        "id": {"type": "integer"},
        "title": {"type": "string"},
        "url": {"type": "string"},
        "thumbnailUrl": {"type": "string"},
    },
    "required": ["albumId", "id", "title", "url", "thumbnailUrl"],
}

GET_TODOS = {
    "type": "array",
    "items": [
        {
            "type": "object",
            "properties": {
                "userId": {"type": "integer"},
                "id": {"type": "integer"},
                "title": {"type": "string"},
                "completed": {"type": "boolean"},
            },
            "required": ["userId", "id", "title", "completed"],
        }
    ],
}

GET_TODO = {
    "type": "object",
    "properties": {
        "userId": {"type": "integer"},
        "id": {"type": "integer"},
        "title": {"type": "string"},
        "completed": {"type": "boolean"},
    },
    "required": ["userId", "id", "title", "completed"],
}

GET_USERS = {
    "type": "array",
    "items": [
        {
            "type": "object",
            "properties": {
                "id": {"type": "integer"},
                "name": {"type": "string"},
                "username": {"type": "string"},
                "email": {"type": "string"},
                "address": {
                    "type": "object",
                    "properties": {
                        "street": {"type": "string"},
                        "suite": {"type": "string"},
                        "city": {"type": "string"},
                        "zipcode": {"type": "string"},
                        "geo": {
                            "type": "object",
                            "properties": {
                                "lat": {"type": "string"},
                                "lng": {"type": "string"},
                            },
                            "required": ["lat", "lng"],
                        },
                    },
                    "required": ["street", "suite", "city", "zipcode", "geo"],
                },
                "phone": {"type": "string"},
                "website": {"type": "string"},
                "company": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "catchPhrase": {"type": "string"},
                        "bs": {"type": "string"},
                    },
                    "required": ["name", "catchPhrase", "bs"],
                },
            },
            "required": [
                "id",
                "name",
                "username",
                "email",
                "address",
                "phone",
                "website",
                "company",
            ],
        }
    ],
}

GET_USER = {
    "type": "object",
    "properties": {
        "id": {"type": "integer"},
        "name": {"type": "string"},
        "username": {"type": "string"},
        "email": {"type": "string"},
        "address": {
            "type": "object",
            "properties": {
                "street": {"type": "string"},
                "suite": {"type": "string"},
                "city": {"type": "string"},
                "zipcode": {"type": "string"},
                "geo": {
                    "type": "object",
                    "properties": {
                        "lat": {"type": "string"},
                        "lng": {"type": "string"},
                    },
                    "required": ["lat", "lng"],
                },
            },
            "required": ["street", "suite", "city", "zipcode", "geo"],
        },
        "phone": {"type": "string"},
        "website": {"type": "string"},
        "company": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "catchPhrase": {"type": "string"},
                "bs": {"type": "string"},
            },
            "required": ["name", "catchPhrase", "bs"],
        },
    },
    "required": [
        "id",
        "name",
        "username",
        "email",
        "address",
        "phone",
        "website",
        "company",
    ],
}
