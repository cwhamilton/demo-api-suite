# [JSONPlaceholder](https://jsonplaceholder.typicode.com/) Tests

## [API Functionality Tests](api_tests)

* Tests the JSONPlaceholder API for:

    * Albums:
        * GET /albums
        * GET /albums/{albumId}
    * Comments
        * GET /comments
        * GET /comments?postId={postId}
        * POST /comments?postId={postId}
    * Photos
        * GET /photos
        * GET /photos/{photoId}
    * Todos
        * GET /todos
        * GET /todos/{todoId}
    * Posts
        * GET /posts
        * GET /posts/id
        * GET /posts/id/comment

* Validates:
    * Response status code
    * Response headers
    * Response body schema
    * Response time

To run these tests use the following command:

    pytest -m jsonph

---------------

## [Load Tests](LocustTests)

* Tests JSONPlaceholder API for load.

Using [locust.io](http://locust.io) feature for Python, we can simulate load on a server with any number of users
accessing their API's simultaneously.

Locust is a scalable load testing tool that uses simple Python language. Built using
the [requests](https://pypi.org/project/requests/) plugin, it provides familiar code to an already existing API suite
without having to keep a separate codebase around.

To run these load tests, use the following command:

    -f PythonAPISuite/JSONPlaceholder/LocustTests/locust.py --host https://jsonplaceholder.typicode.com

Once running, access [localhost:8089](http://localhost:8089/), set the number of users you want to run against the
server and how quickly you want them to ramp. Once started it will log metrics for all requests made and provide graphs.

## Note:

I only wrote a few tasks/tags in there because I do not own or pay for the free server that is JSONPlaceholder. I do not
want to put undue strain on them so capped it at a small example. When running please keep this in mind and keep the
number of users to a small.