import pytest
import requests

from PythonAPISuite.JSONPlaceholder import jsonph
from PythonAPISuite.base import env_settings, http
from PythonAPISuite.base.util import Settings, Util


@pytest.fixture(autouse=True)
def setup_test_context(request):
    request.cls.context = "JSONPH"
    Settings.environment = env_settings.JSONPH_ENV
    yield
    Settings.environment = None


#############
### Users ###
#############


@pytest.fixture()
def get_users() -> requests.Response:
    """
    Returns a list of all users
    """
    response = http.get("/users")
    Util.is_response_expected_status_code(response, 200)

    return response


@pytest.fixture()
def get_user_id(get_users: requests.Response) -> int:
    """
    Returns the first user ID from get_users response
    """
    yield get_users.json()[0].get("id")


@pytest.fixture()
def get_user(get_user_id: int) -> requests.Response:
    """
    Returns the information on a single user by ID
    """
    response = http.get(f"/users/{get_user_id}")
    Util.is_response_expected_status_code(response, 200)

    yield response


#############
### Posts ###
#############


@pytest.fixture()
def get_posts() -> requests.Response:
    """
    Returns a list of all posts
    """
    response = http.get("/posts")
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def get_post_id(get_posts: requests.Response) -> int:
    """
    Returns the first post ID from get_posts response
    """
    yield get_posts.json()[0].get("id")


@pytest.fixture()
def get_post(get_post_id: int) -> requests.Response:
    """
    Returns the response of a GET request on a single post ID
    """
    response = http.get(f"/posts/{get_post_id}")
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def create_post(get_user_id: int) -> requests.Response:
    """
    Creates a post using a user ID.
    Deletes that post after the test is run.
    """
    response = jsonph.create_post(get_user_id)
    Util.is_response_expected_status_code(response, 201)

    yield response


@pytest.fixture()
def put_post(get_post_id: int, get_user_id: int) -> requests.Response:
    """
    Modifies an existing post body and title by ID
    """
    var = Util.get_unique_string()
    request_body = {
        "title": f"title_{var}",
        "body": f"body_{var}",
        "userId": get_user_id,
    }

    response = http.put(f"/posts/{get_post_id}", json=request_body)
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def patch_post(get_post_id: int, get_user_id: int) -> requests.Response:
    """
    Updates a given post title
    """
    var = Util.get_alpha_num()
    request_body = {"title": f"title_{var}"}

    response = http.patch(f"/posts/{get_post_id}", json=request_body)
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def delete_post(get_post_id: int) -> requests.Response:
    """
    Deletes a given post by ID
    """
    response = http.delete(f"/posts/{get_post_id}")
    Util.is_response_expected_status_code(response, 200)

    yield response


################
### Comments ###
################


@pytest.fixture()
def get_comments() -> requests.Response:
    """
    Returns all comments
    """
    response = http.get("/comments")
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def get_post_comments(get_post_id: int) -> requests.Response:
    """
    Returns all the comments on a single post
    """
    response = http.get(f"/posts/{get_post_id}/comments")
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def get_comment_by_post_id(get_post_id: int) -> requests.Response:
    """
    Returns all comments by including post ID in the request body
    """
    response = http.get("/comments", params={"postId": get_post_id})
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def create_comment(get_post_id: int, get_user_id: int) -> requests.Response:
    """
    Creates a comment on a post
    """
    response = http.post(
        f"/comments",
        params={
            "postId": get_post_id,
            "userId": get_user_id,
            "body": f"body_{Util.get_unique_string()}",
        },
    )
    Util.is_response_expected_status_code(response, 201)

    yield response


##############
### Albums ###
##############


@pytest.fixture()
def get_albums() -> requests.Response:
    """
    Returns all albums
    """
    response = http.get(f"/albums")
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def get_album_id(get_albums: requests.Response) -> int:
    """
    Returns the first album ID from get_albums
    """
    yield get_albums.json()[0].get("id")


@pytest.fixture()
def get_album(get_album_id) -> requests.Response:
    """
    Returns album info by ID
    """
    response = http.get(f"/albums/{get_album_id}")
    Util.is_response_expected_status_code(response, 200)

    yield response


##############
### Photos ###
##############


@pytest.fixture()
def get_photos() -> requests.Response:
    """
    Returns all photos
    """
    response = http.get(f"/photos")
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def get_photo_id(get_photos: requests.Response) -> int:
    """
    Returns first photo ID
    """
    yield get_photos.json()[0].get("id")


@pytest.fixture()
def get_photo(get_photo_id: int) -> requests.Response:
    """
    Returns information on a specific photo by ID
    """
    response = http.get(f"/photos/{get_photo_id}")
    Util.is_response_expected_status_code(response, 200)

    yield response


##############
### To Dos ###
##############


@pytest.fixture()
def get_todos() -> requests.Response:
    """
    Returns all ToDos
    """
    response = http.get(f"/todos")
    Util.is_response_expected_status_code(response, 200)

    yield response


@pytest.fixture()
def get_todo_id(get_todos: requests.Response) -> int:
    """
    Returns the ID of the first to do from get_todos
    """
    yield get_todos.json()[0].get("id")


@pytest.fixture()
def get_todo(get_todo_id: requests.Response) -> requests.Response:
    """
    Returns information on a single To Do by ID
    """
    response = http.get(f"/todos/{get_todo_id}")
    Util.is_response_expected_status_code(response, 200)

    yield response
