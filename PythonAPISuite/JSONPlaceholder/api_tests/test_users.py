import pytest

from PythonAPISuite.JSONPlaceholder.schemas import GET_USERS, GET_USER
from PythonAPISuite.base import assertions, http

"""
users:
    GET     /users
    GET     /users/{userId}
"""


@pytest.mark.jsonph
@pytest.mark.users
class TestUsers:

    ####################
    ### GET Requests ###
    ####################

    def test_jsonph_get_users(self, get_users):
        """
        Validate GET /users response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_users, 200)
        assertions.validate_response_schema(get_users, GET_USERS)
        assertions.validate_response_headers(get_users)
        assertions.validate_response_time(get_users)

    def test_jsonph_get_users_id(self, get_user):
        """
        Validate GET /users/{userId} response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_user, 200)
        assertions.validate_response_schema(get_user, GET_USER)
        assertions.validate_response_headers(get_user)
        assertions.validate_response_time(get_user)

    def test_jsonph_get_users_invalid_id(self):
        """
        Validate GET /users/invalid_id response Code
        """
        response = http.get("/user/invalidId")

        assertions.validate_response_code(response, 404)
