import pytest

from PythonAPISuite.JSONPlaceholder.schemas import GET_TODOS, GET_TODO
from PythonAPISuite.base import assertions, http

"""
todos:
    GET     /todos
    GET     /todos/{todoId}
"""


@pytest.mark.jsonph
@pytest.mark.todos
class TestTodos:

    ####################
    ### GET Requests ###
    ####################

    def test_jsonph_get_todos(self, get_todos):
        """
        Validate GET /todos response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_todos, 200)
        assertions.validate_response_schema(get_todos, GET_TODOS)
        assertions.validate_response_headers(get_todos)
        assertions.validate_response_time(get_todos)

    def test_jsonph_get_todos_id(self, get_todo):
        """
        Validate GET /todos/{todoId} response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_todo, 200)
        assertions.validate_response_schema(get_todo, GET_TODO)
        assertions.validate_response_headers(get_todo)
        assertions.validate_response_time(get_todo)

    def test_jsonph_get_todo_invalid_id_response_code(self):
        """
        Validate GET /todos/invalid_id response Code
        """
        response = http.get("/todos/invalidId")

        assertions.validate_response_code(response, 404)
