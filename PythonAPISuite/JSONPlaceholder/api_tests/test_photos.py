import pytest

from PythonAPISuite.JSONPlaceholder.schemas import GET_PHOTOS, GET_PHOTO
from PythonAPISuite.base import assertions, http

"""
Photos:
    GET     /photos
    GET     /photos/{photoId}
"""


@pytest.mark.jsonph
@pytest.mark.photos
class TestPhotos:

    ####################
    ### GET Requests ###
    ####################

    def test_jsonph_get_photos(self, get_photos):
        """
        Validate GET /photos response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_photos, 200)
        assertions.validate_response_schema(get_photos, GET_PHOTOS)
        assertions.validate_response_headers(get_photos)
        assertions.validate_response_time(get_photos)

    def test_jsonph_get_photos_id(self, get_photo):
        """
        Validate GET /photos/{photoId} response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_photo, 200)
        assertions.validate_response_schema(get_photo, GET_PHOTO)
        assertions.validate_response_headers(get_photo)
        assertions.validate_response_time(get_photo)

    def test_jsonph_get_photos_invalid_id(self):
        """
        Validate the GET /photos/invalid_id response Code
        """
        response = http.get("/photos/invalidId")

        assertions.validate_response_code(response, 404)
