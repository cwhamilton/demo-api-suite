import pytest

from PythonAPISuite.JSONPlaceholder.schemas import GET_ALBUMS, GET_ALBUM
from PythonAPISuite.base import assertions, http

"""
Albums:
    GET     /albums
    GET     /albums/{albumId}
"""


@pytest.mark.jsonph
@pytest.mark.albums
class TestAlbums:

    ####################
    ### GET Requests ###
    ####################

    def test_jsonph_get_albums(self, get_albums):
        """
        Validate GET /albums response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_albums, 200)
        assertions.validate_response_schema(get_albums, GET_ALBUMS)
        assertions.validate_response_headers(get_albums)
        assertions.validate_response_time(get_albums)

    def test_jsonph_get_albums_id(self, get_album):
        """
        Validate GET /albums/{albumId} response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_album, 200)
        assertions.validate_response_schema(get_album, GET_ALBUM)
        assertions.validate_response_headers(get_album)
        assertions.validate_response_time(get_album)

    def test_jsonph_get_album_invalid_id_response_code(self):
        """
        Validate GET /albums/invalid_id response Code
        """
        response = http.get(f"/album/invalid_id")

        assertions.validate_response_code(response, 404)
