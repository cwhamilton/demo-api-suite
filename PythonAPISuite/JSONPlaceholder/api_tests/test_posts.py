import pytest

from PythonAPISuite.JSONPlaceholder.schemas import (
    GET_POSTS,
    GET_POST,
    GET_POST_COMMENTS,
    PUT_POST,
)
from PythonAPISuite.base import assertions, http

"""
Posts
    GET 	/posts
    GET 	/posts/{postId}
    GET 	/posts/{postId}/comments
    POST 	/posts
    PUT 	/posts/{postId}
    PATCH 	/posts/{postId}
    DELETE 	/posts/{postId}
"""


@pytest.mark.jsonph
@pytest.mark.posts
class TestPosts:

    ####################
    ### GET Requests ###
    ####################

    def test_jsonph_get_posts(self, get_posts):
        """
        Validate GET /posts response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_posts, 200)
        assertions.validate_response_headers(get_posts)
        assertions.validate_response_schema(get_posts, GET_POSTS)
        assertions.validate_response_time(get_posts)

    def test_jsonph_get_posts_id(self, get_post):
        """
        Validate GET /posts/{postId} response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_post, 200)
        assertions.validate_response_headers(get_post)
        assertions.validate_response_schema(get_post, GET_POST)
        assertions.validate_response_time(get_post)

    def test_jsonph_get_posts_invalid_id(self):
        """
        Validate GET /posts/invalid_id response Code
        """
        response = http.get("/posts/invalidId")

        assertions.validate_response_code(response, 404)

    def test_jsonph_get_posts_comment_id(self, get_post_comments):
        """
        Validate GET /posts/{postId}/comments response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_post_comments, 200)
        assertions.validate_response_headers(get_post_comments)
        assertions.validate_response_schema(get_post_comments, GET_POST_COMMENTS)
        assertions.validate_response_time(get_post_comments)

    def test_jsonph_get_posts_comment_invalid_id(self):
        """
        Validates a 200 response and empty body are returned when an invalid postId is sent
        """
        response = http.get("/posts/invalidId/comments")

        assertions.validate_response_code(response, 200)
        assert response.json() == []

    #####################
    ### POST Requests ###
    #####################

    def test_jsonph_post_posts(self, create_post):
        """
        Validate POST /posts response Code, Headers, and Time
        """
        assertions.validate_response_code(create_post, 201)
        assertions.validate_response_headers(create_post)
        assertions.validate_response_time(create_post)

    @pytest.mark.xfail(reason="Known issue, returns 200 instead of 400")
    def test_jsonph_post_posts_response_body_invalid_empty_title(self, get_user_id):
        """
        Validates response code when an empty title is sent with POST /posts
        """
        create_post = http.post(
            "/posts", json={"title": None, "body": "body", "userId": get_user_id}
        )

        assertions.validate_response_code(create_post, 400)

    @pytest.mark.xfail(reason="Known issue, returns 200 instead of 400")
    def test_jsonph_post_posts_response_body_invalid_body(self, get_user_id):
        """
        Validates response code when an empty body is sent with POST /posts
        """
        create_post = http.post(
            "/posts", json={"title": "Title", "body": None, "userId": get_user_id}
        )

        assertions.validate_response_code(create_post, 400)

    @pytest.mark.xfail(reason="Known issue, returns 200 instead of 400")
    def test_jsonph_post_posts_response_body_invalid_user_id(self):
        """
        Validates response code when an empty User ID is sent with POST /posts
        """
        create_post = http.post(
            "/posts", json={"title": "Title", "body": "Body", "userId": "InvalidId "}
        )

        assertions.validate_response_code(create_post, 400)

    ####################
    ### PUT Requests ###
    ####################

    def test_jsonph_put_posts_response_code(self, put_post):
        """
        Validate PUT /posts response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(put_post, 200)
        assertions.validate_response_headers(put_post)
        assertions.validate_response_schema(put_post, PUT_POST)
        assertions.validate_response_time(put_post)

    ######################
    ### PATCH Requests ###
    ######################

    def test_jsonph_patch_posts_response_code(self, patch_post):
        """
        Validate PATCH /posts response Code, Headers, Schema, and Time
        Note: Schema matches PUT request, reused that.
        """
        assertions.validate_response_code(patch_post, 200)
        assertions.validate_response_headers(patch_post)
        assertions.validate_response_schema(patch_post, PUT_POST)
        assertions.validate_response_time(patch_post)

    #######################
    ### DELETE Requests ###
    #######################

    def test_jsonph_delete_posts_response_code(self, delete_post):
        """
        Validate PATCH /posts response Code, Headers, Schema, Time, and Body
        No response body content expected, will not validate schema.
        """
        assertions.validate_response_code(delete_post, 200)
        assertions.validate_response_headers(delete_post)
        assertions.validate_response_time(delete_post)
        assert not delete_post.json()

    #######################
    ### Invalid Methods ###
    #######################

    @pytest.mark.parametrize("methods", [http.delete, http.patch])
    @pytest.mark.xfail(reason="Known issue, returns 404 instead of 405")
    def test_jsonph_invalid_methods_posts(self, methods):
        response = methods("/posts")

        assertions.validate_response_code(response, 405)

    @pytest.mark.parametrize("methods", [http.post])
    @pytest.mark.xfail(reason="Known issue, returns 404 instead of 405")
    def test_jsonph_invalid_methods_posts_id(self, methods):
        response = methods("/posts/1")

        assertions.validate_response_code(response, 405)

    @pytest.mark.parametrize("methods", [http.put, http.patch])
    @pytest.mark.xfail(reason="Known issue, returns 404 instead of 405")
    def test_jsonph_invalid_methods_posts_id_comments(self, methods):
        response = methods("/posts/1/comments")

        assertions.validate_response_code(response, 405)
