import pytest

from PythonAPISuite.JSONPlaceholder.schemas import GET_COMMENTS
from PythonAPISuite.base import http, assertions

"""
Comments:
    GET 	/comments
    GET 	/comments?postId={postId}
    POST 	/comments?postId={postId}
    PUT 	/comments/{commentId}?postId={postId}
    PATCH 	/comments/{commentId}?postId={postId}
    DELETE 	/comments/{commentId}?postId={postId}
"""


@pytest.mark.jsonph
@pytest.mark.comments
class TestComments:

    ####################
    ### GET Requests ###
    ####################

    def test_jsonph_get_comments(self, get_comments):
        """
        Validate GET /comments response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_comments, 200)
        assertions.validate_response_schema(get_comments, GET_COMMENTS)
        assertions.validate_response_headers(get_comments)
        assertions.validate_response_time(get_comments)

    def test_jsonph_get_comments_invalid_post_id(self):
        """
        Validate GET /comments with an invalid postID response Code and Body
        """
        response = http.get("/comments", params={"postId": "invalid_id"})

        assertions.validate_response_code(response, 200)
        assert response.json() == [], f"Response body not empty."

    def test_jsonph_get_comments_invalid_param(self):
        """
        Validate GET /comments with an invalid params response Code
        Endpoint ignores invalid params, 200 expected.
        """
        assertions.validate_response_code(
            http.get("/comments", params={"invalid": 1}), 200
        )

    def test_jsonph_get_comments_by_post_id(self, get_comment_by_post_id):
        """
        Validate GET /comments?postId={postId} response Code, Headers, Schema, and Time
        """
        assertions.validate_response_code(get_comment_by_post_id, 200)
        assertions.validate_response_schema(get_comment_by_post_id, GET_COMMENTS)
        assertions.validate_response_headers(get_comment_by_post_id)
        assertions.validate_response_time(get_comment_by_post_id)

    def test_jsonph_get_comments_by_post_invalid_id(self):
        """
        Validate GET /comments?postId=invalidId response Code and Body
        """
        response = http.get("/comments", params={"postId": "invalidId"})

        # 200 expected with empty response
        assertions.validate_response_code(response, 200)
        assert response.json() == [], "Response body not empty"

    #####################
    ### POST Requests ###
    #####################

    def test_jsonph_post_comment_response_code(self, get_comments, create_comment):
        """
        Validate POST /comments response Code, Headers, Time, and Body
        Note: Only an ID is returned.  Will validate against that instead of checking schema.
        """
        assertions.validate_response_code(create_comment, 201)
        assertions.validate_response_headers(create_comment)
        assertions.validate_response_time(create_comment)
        assert (
            create_comment.json().get("id") == len(get_comments.json()) + 1
        ), "Returned ID did not match expected value"
