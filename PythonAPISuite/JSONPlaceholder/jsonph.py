import requests

from PythonAPISuite.base import http
from PythonAPISuite.base.util import Util


def create_post(
    user_id: int = None, title: str = None, body: str = None
) -> requests.Response:
    """
    Creates a new post by a user.
    """
    var = Util.get_alpha_num(5)

    if not user_id:
        user_id = http.get("/users").json()[0].get("id")
    if not title:
        title = f"title_{var}"
    if not body:
        body = f"body_{var}"

    request_body = {"title": title, "body": body, "userId": user_id}

    return http.post("/posts", json=request_body)
