import os
import shutil

import pytest

from PythonAPISuite.base.util import DataLoader


@pytest.fixture(scope="session")
def setup_temp_dir():
    cwd = os.getcwd()
    temp_folder = f"{cwd}/{DataLoader.TMP_FOLDER}"

    # Open temp folder
    if not os.path.exists(temp_folder):
        os.mkdir(temp_folder, 755)

    yield

    cwd = os.getcwd()
    temp_folder = f"{cwd}/{DataLoader.TMP_FOLDER}"

    # Close temp folder
    if os.path.exists(temp_folder):
        shutil.rmtree(temp_folder)


def pytest_addoption(parser):
    parser.addoption("--env")
    parser.addoption(
        "--runslow",
        action="store_true",
        default=False,
        help="Includes slow api_tests in test run",
    )
    parser.addoption(
        "--quarantined",
        action="store_true",
        default=False,
        help="Includes quarantined api_tests in test run",
    )


def pytest_collection_modifyitems(config, items):
    # Run api_tests marked 'slow'
    if config.getoption("--runslow"):
        return

    skip_slow = pytest.mark.skip(reason="Need --runslow option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)

    # Run api_tests marked 'quarantined'
    if config.getoption("--quarantined"):
        return

    skip_quarantined = pytest.mark.skip(reason="Need --quarantined option to run")
    for item in items:
        if "quarantined" in item.keywords:
            item.add_marker(skip_quarantined)
