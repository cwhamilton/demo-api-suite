# Instructions:
### Requirements
* Currently, supports [Python3.10](https://www.python.org/downloads/release/python-3100/)
* Visit [requirements.txt](requirements.txt) to see plugin requirements
* To install quickly, use the following:


    pip3 install -r requirements.txt

Or simply open the requirements file in [PyCharm](https://www.jetbrains.com/pycharm/download/) and right click and select 'Install All Packages' 

![](Screenshots/requirements.png)

###### Run All Tests:
    pytest

The environment will automatically be selected based on the folder the `tests` folder is in.  To further filter the tests, see below.

##### BASICS
    -s           | Capture=no
    -v           | --verbose.  More logs
    -q           | --quiet.  No logs
    -x           | Stop after the first failure.
    -n=x         | Number of threads to run tests on.  Beware This can cause concurrency issues.
    -rxXs        | Show extra info on xfailed, xpassed, and skipped tests
    --duration s | Logs the duration of all tests.  Reports the slowest 
    --fixtures   | Will list out all fixtures
    --markers    | Lists all markers from pytest.ini file
    --count=x    | Runs the entire test suite x times

###### Run Subsets
    -m <str>        | Markers.  Runs a subset of tests.  Can be singular or use operators to filter further.
                       jsonph
                      "posts and comments"
                      "posts not comments"
    --quarantined   | Will run tests that can break things dangerously 
    --runslow       | Tests marked 'slow' will be omitted by default.  Including this in CLI will run these.
    
###### pytest Markers
    xfail:  Test is one that is expected to fail.  Will not count against total failures.  If it passes, it will be repoted as 'xpassed'
    skip:   Skip a test.       
    

###### JSONPlaceholder:
    pytest -m jsonph
